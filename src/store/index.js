import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 决定是否显示登录框
    isShowLoginBox:false,
    // 这个数据是toast组件的 提示信息
    toastMsg : '生日快乐',
    // 这个数据是 是否显示 toast组件
    isShowToast:false,
    // 提示组件的默认图标
    toastIcon : 'icon-toast_chenggong',
    // 全局存储的用户数据
    userInfo : {},
    // 给一个key给tophead组件，让里面的数据在需要的时候更新
    topHeadKey:0
  },
  mutations: {
    updateTopHeadKey(state){
      state.topHeadKey++
    },
    updateUserInfo(state,payload){
      state.userInfo = payload
    },
    // 修改显示登录框的状态
    modifyShowLogin(state,payload){
      state.isShowLoginBox = payload
    },
    // 也可以写成下面这样
    // showLoginBox(){
    //   state.isShowLoginBox = true
    // },
    // hideLoginBox(){
    //   state.isShowLoginBox = false
    // }

    // 用于决定 toast是否显示的一个方法
    showToast(state,payload){
      // payload 可以是一个对象 ， 包含 提示文字：msg , 提示的类型 : type
      state.isShowToast = true
      state.toastMsg = payload.msg
      state.toastIcon = payload.type
    },
    hideToast(state){
      state.isShowToast = false
    }
  },
  actions: {
  },
  modules: {
  }
})
