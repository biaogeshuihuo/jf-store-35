// 因为要调用stroe的方法，所以把store导入
import store from '../store'
// 我们在使用toast组件的时候发现，使用起来太麻烦了，但是是有一定的规律的，把规律封装起来
export function toast(msg,type){
    // 判断type 是 success 还是 dander 还是 warnning
    //              成功        失败        警告
    switch(type){
        case 'dander':
            type = 'icon-toast-shibai_huaban'
        break;
        case 'warnning':
            type = 'icon-toast-jinggao'
        break;
        default:
            type = 'icon-toast_chenggong'
    }
    // 先显示
    store.commit('showToast',{msg,type})
    // 等待一会，隐藏
    setTimeout(() => {
        store.commit('hideToast')
    }, 1500);
}