import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 需要在main里面把 reset-css 导入，进行样式初始化
import "reset-css"
import "./assets/common/common.less"
import "./utils/filters" // 如果只是执行一个js，不用 from

// 拖动验证的插件，需要先注册
import SlideVerify from 'vue-monoplasty-slide-verify';
// 在vue中使用插件就是 Vue.use(插件)
Vue.use(SlideVerify)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
