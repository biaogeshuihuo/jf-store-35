import axios from 'axios'


// 创建一个统一的实例，让整个网站的所有请求都有一些相同的配置
const instance = axios.create({
    // 全局统一的接口服务器地址
    baseURL: "http://gz.wolfcode.cn/cms",
    // 统一配置请求超时
    timeout : 5000,
})

const urlencodedRequest = ['/sendSMS','/phoneRegin','/wechatUsers/PCLogin' ,'/shop/carts/add']

// 设置请求拦截器 - 会在请求发送出去之前执行
instance.interceptors.request.use(config=>{
    // config 就是我们所有设置的 axios(config)
    // console.log(config);
    // 当我们请求的是 获取 短信验证码的时候，请求头要求是 urlencoded
    // if(config.url === '/sendSMS' || config.url === '/phoneRegin' || config.url === '/wechatUsers/PCLogin'){
    if(urlencodedRequest.includes(config.url)){
        config.headers['Content-Type'] = 'application/x-www-form-urlencoded'
    }
    // 如果已经登录，就把请求头里面加上token
    const token = localStorage.getItem('x-auth-token')
    if(token){
        config.headers['x-auth-token'] = token
    }
    // 将来还会 在这带上 token
    return config
})

// 响应拦截器
instance.interceptors.response.use(res=>{
    // 一般在响应拦截里面做一些数据加工
    return res.data
})

export default instance