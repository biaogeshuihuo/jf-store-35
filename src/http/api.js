import request from './request'

// 上班的时候，习惯于把请求的代码，都封装成一个api的文件 , 统一管理所有的请求
export const getVerifyApi = phone=>request.post('/sendSMS',`phone=${phone}`)

// 这个才是我们以后写api.js的标准写法
export const phoneLoginApi = params => request.post('/phoneRegin', qs.stringify(params))
// josn 格式： {"key":"value","key":'value'}   --- axios 默认的
// ulrencoded格式 key=value&key=value          --- jq默认的

import qs from 'qs'
export const getUserInfoApi = ()=> request.get('/shop/userProfiles')


export const WXLoginApi = code=>request.post('/wechatUsers/PCLogin',qs.stringify({code}))

// 获取精品推荐的数据
export const getRecommendApi = ()=>request.get('/products/recommend')
export const getHotListApi = ()=>request.get('/products/hot')

// get请求，axios要求我们使用 { params : { id: 1, name: ''} } 这个格式传递数据
export const getMoreApi = params=>request.get('/products',{params})
 
export const getGoodDetailApi = params=>request.get(`/products/${params}`)

export const addToCarApi = params=>request.post('/shop/carts/add',qs.stringify(params))

export const getCartDataApi = ()=>request.get('/shop/carts')

// 在axios里面，使用的 http 2.0 的标准
// 在http 1.0 里面,只有get、post两个请求方式，后来觉得不够
// 在http 2.0 里面新增两个请求方式 ：put / delete
// get - 查 ， post - 增 ， put - 改 , delete - 删
export const deleteCartDataApi = id=> request.delete(`/shop/carts?productIds=${id}`)