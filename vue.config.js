/*
    之前使用webpack 需要写一个 webpack.config.js
    做webpack的配置的
    vue-cli基于webpack的
    vue-cli保留了我们自己修改webpack配置的功能
    如果是使用vue-cli实现项目，要写的配置文件就是 vue.config.js
*/
const path = require("path");
function resolve(dir) {
    return path.join(__dirname, dir);
}

module.exports = {
    // 是当前的脚手架使用的开发服务器的配置
    devServer: {
        // 自己打开浏览器
        open: true
    },
    chainWebpack: config => {
        config.resolve.alias
            .set("@", resolve("src"))
            .set("assets", resolve("src/assets"))
            .set("components", resolve("src/components"))
            .set("base", resolve("baseConfig"))
            .set("public", resolve("public"));
    },
}